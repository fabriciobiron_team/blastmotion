<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class EntryController 
{

    public function index()
    {
        return response()->json(Entry::all());
    }
    
    public function store(Request $request)
    {
        // dd($request->all());

        
        $validation = $request->validate([
            'title' => 'required|max:255',
            'body' => 'required'
        ]);

        $entry = new Entry();
        $entry->title = $request->title;
        $entry->excerpt = $request->excerpt;
        $entry->body = $request->body;
        $entry->save(); 
        
        return response()->json($entry );
    }

    public function show($id)
    {
        $entry = Entry::find($id);

        return response()->json($entry);
    }

    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'title' => 'required|max:255',
            'body' => 'required'
        ]);

        $entry = Entry::find($id);
        $entry->title = $request->title;
        $entry->excerpt = $request->excerpt;
        $entry->body = $request->body;
        $entry_id = $entry->save();

        return response()->json($entry);
    }

    public function destroy($id)
    {   
        $entry = Entry::find($id);
        $entry->delete();
        return response()->json($entry);
        
    }

    public function upload(Request $request)
    {

        $path = Storage::putFile('storage', $request->file('file'),'public');

        $entry = Entry::find($request->id);

        $entry->featured_image = $path;

        $entry->save();

        return response()->json($entry);
    }

}
