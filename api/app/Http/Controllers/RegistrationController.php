<?php

namespace App\Http\Controllers;

use App\Registration;
use Illuminate\Http\Request;

use App\Mail\AuthorizationMail;
use Illuminate\Mail\Mailable;
use App\Http\Requests\RegistrationStoreValidation as Validation;



class RegistrationController
{

    public function index()
    {
        $registrations = Registration::all();
        return response()->json($registrations);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => ['required','email'],
        ]);

        $registration = new Registration();
        $registration->email = $request->email;
        $registration->token = uniqid();
        $registration->save();  

        $mail = new AuthorizationMail($registration);
        \Mail::to($registration->email)->send($mail); 
        return response()->json($registration);

    }
   
    public function authorize($token)
    {
        $registration = Registration::where('token',$token)->first();
        $registration->authorized = 1;
        $registration->save(); 

        return view('registered',$registration);
    }

    public function destroy(Registration $registration)
    {
        //
    }

}
