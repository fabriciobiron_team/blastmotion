<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuthorizationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $params;

    public function __construct($params) 
    {
        $this->params = $params;
    } 

    public function build()
    {   
        return $this -> from('fabriciobiron@gmail.com','Blast Motion')
        ->view('email')
        ->with([
            'registration' => $this->params,                             
        ]);
    }
}
