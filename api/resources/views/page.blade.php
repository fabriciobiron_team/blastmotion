<h1>Blast Motion presents Blast Coach</h1>
<h2>State of the art motion capture and Artificial Intelligence</h2>
<p>Blast Motion presents its state of the art AI powered golf coach. Gain insights into your
performance using key metrics and industry leading sensor technology.</p>
<p> We develop state of the art intelligent inertial motion sensors, 3D motion analysis
algorithms / signal processing, smartphone based apps with 3D visualization and machine learning,
and cloud computing for advanced analytics via a team / coach oriented web portal. Through this
chain of technology and refined solutions, our goal is to provide any aspiring or pro athlete a
simple yet profound UX that enables them to improve rapidly through well founded sports
science.</p>
<form>
<p>Enter your email to be notified when we release.</p>
<input name=”email”>
</form>