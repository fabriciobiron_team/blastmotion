<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Auth::routes();

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return response()->json($request->user());
});

Route::middleware('auth:api')->get('/entries', 'EntryController@index');
Route::middleware('auth:api')->post('/entries', 'EntryController@store');
Route::middleware('auth:api')->get('/entries/{id}', 'EntryController@show');
Route::middleware('auth:api')->put('/entries/{id}', 'EntryController@update');
Route::middleware('auth:api')->delete('/entries/{id}', 'EntryController@destroy');
Route::middleware('auth:api')->post('/entries/upload', 'EntryController@upload');


Route::middleware('auth:api')->get('/registrations', 'RegistrationController@index');
Route::middleware('auth:api')->post('/registrations', 'RegistrationController@store');


Route::middleware('api')->get('/home', 'HomeController@index');



