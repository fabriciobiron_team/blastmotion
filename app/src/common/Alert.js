var Alert = {};

Alert.install = function(Vue, options){
    
    let emitter = options.emitter

    let component = Vue.component('alertMessages', {
        template: '<div v-show="alertVisible" ref="alertComponent"><div :class="`alert alert-${type}`">{{message}}</div></div>',
        data:function(){
            return{
                alertVisible: false,
                type: '',
                message: ''
            }
        },
        methods:{
            showAlert:function(params){
                this.type = (typeof params.type === 'undefined') ? 'no status' : params.type
                this.message = (typeof params.message === 'undefined') ? 'no message' : params.message
                this.alertVisible = true
                this.closeAlert()
            },
            closeAlert:function(){
                setTimeout(()=>{
                    this.alertVisible = false
                },3000)
            }
        },
        created:function(){
            emitter.$on('alert-show',(params)=>{
                this.showAlert(params);
            })
        }
    })

	let methods = {
		show:function(params){
            emitter.$emit('alert-show',params)
        },
        
    };

	Vue.prototype.$alert = methods;
};

export default Alert;