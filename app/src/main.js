// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

//Components
import EntryCreate from './components/EntryCreate.vue'
import EntryForm from './components/EntryForm.vue'
import EntryEditForm from './components/EntryEditForm.vue'
import EntryItem from './components/EntryItem.vue'
import EntriesList from './components/EntriesList.vue'
import RegistrationList from './components/RegistrationList.vue'
import Profile from './components/Profile.vue'
import UploadImage from './components/UploadImage.vue'

Vue.config.productionTip = false

import  VueCookie from 'vue-cookie'
Vue.use(VueCookie)

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import Alert from './common/Alert'
import Emitter from './common/emitter'
Vue.use(Alert,{emitter: Emitter}) 

Vue.component('entry-create',EntryCreate)
Vue.component('entry-form',EntryForm)
Vue.component('entry-edit-form',EntryEditForm)
Vue.component('entry-item',EntryItem)
Vue.component('entries-list',EntriesList)
Vue.component('registration-list',RegistrationList)
Vue.component('profile',Profile)
Vue.component('upload-image',UploadImage)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
