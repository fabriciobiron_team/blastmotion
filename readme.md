# Blast Motion

## API
This is a API that uses OAuth 2.0.

### Setting API Address
This is the API Address: http://www.fabriciobiron.me/blastmotion/api/public/api 

### Getting authorization 
http://www.fabriciobiron.me/blastmotion/api/public/oauth/token

Params
```json
{
  "grant_type": "password",
  "client_id": "3",
  "client_secret": "iLTucIIBhFodS7nTAGzRL5cHmthPe2EFsf8GMxaq",
  "username": "dallard@blastmotion.com",
  "password": "zaq123",
  "scope": "*"
}
```

IMPORTANT: The authorization header is "Bearer". 

----
### Endpoints

##### Entry Model
* GET `/entries`: Lists all available entries
* POST `/entries`: Adds a new entry 
```json
{
  "title": "This is a new title"
}
```
* GET `/entries/{id}`: Retrieves a single entry
* PUT  `/entries/{id}`: Updates the single entry 
```json
{
  "title": "This is a updated title"
}
```
* DELETE  `/entries/{id}`: Deletes the single entry

##### Registration Model
* GET `/registrations`: Lists all available registered emails
* POST `/registrations`: Adds a new email 
```json
{
  "email": "ajackett@blastmotion.com"
}
```
* GET `/authorize/{token}`: Changes status os a registration 

### List of emails  endpoint
http://localhost/blaster/api/public/registrations

## APP (Vue)
`URL:` http://www.fabriciobiron.me/blastmotion/app/#/login

![alt text](blastmotion-vue-app.gif)


#### Application files

##### `/common`
* **fb-fetch-library**: JS Class created by me in order to make a bridge between Laravel API and any JS application, becoming the use soft and easy.
##### `/pages`
* **Dashboard.vue**: Manages the content accessible through login.
* **Home.vue**: Lists all created entries and provides registering user feature  
* **Login**: Provides credentials to access Dashboard 
* **DetailPage**: Shows Entry page.  
##### `/components`
* **EntriesList.vue**: Lists all Entries.
* **EntryItem.vue**: Single entry component.
* **EntryCreate.vue**: Handles form to create an Entry.
* **EntryEditForm.vue**: Handles form to edit an specific entry.
* **Profile.vue**: Shows user info.
* **RegistrationList.vue**: Lists all registered emails.
* **UploadImage.vue**: Handles image upload.        